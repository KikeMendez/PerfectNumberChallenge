<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseApiResponseController as ApiResponse;
use Validator;


class PerfectNumberController extends Controller
{
    /**
     * index function
     *
     * @param Request $request
     */
    public function index(Request $request) {

        $apiResponse = new ApiResponse;

        $validator = Validator::make($request->all(), [
            'number' => 'required|integer|min:1',
        ]);

        if($validator->fails()) {
            return $apiResponse->sendError('Validation Error.', $validator->errors());
        }

        return $this->getDivisors($request->input('number'));
    }

    /**
     * getDivisors
     *
     * @param int $inputNumber
     * @return void
     */
    private function getDivisors($inputNumber) {

        $divisors = [];

        for($i = 1; $i <= sqrt($inputNumber) ; $i++) {

            if($inputNumber % $i == 0) {

                if ($inputNumber / $i == $i) {
                    array_push($divisors, $i);
                }
                else
                {
                    array_push($divisors, $i);
                    array_push($divisors, $inputNumber / $i);
                }
            }

        }

        $result = $this->addDivisores($divisors, $inputNumber);
        return $this->checkResultAndSendResponse($result, $inputNumber);
    }


    /**
     * Add all the dividers
     *
     * @param array $divisors
     * @param int $inputNumber
     * @return int
     */
    private function addDivisores($divisors, $inputNumber){

        $result = NULL;

        foreach($divisors as $divisor){
            $result += $divisor;
        }

        return $result - $inputNumber;
    }

    /**
     * checkResultAndSendResponse
     *
     * @param int $result
     * @param int $inputNumber
     *
     */
    private function checkResultAndSendResponse($result, $inputNumber) {

        $apiResponse = new ApiResponse;

        $message = '';

        if($result == $inputNumber) {

            $message = $inputNumber . ' is a perfect number';

        }elseif($result > $inputNumber) {

            $message = $inputNumber . '  is an abundant number';

        }else {

            $message = $inputNumber . '   is a deficient number';
        }

        $success['total_result']  = $result;
        return $apiResponse->sendResponse($success, $message);
    }

}
