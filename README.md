# Perfect Number Challenge #

We would like you to write a function in PHP to determine whether a given number is [perfect](https://en.wikipedia.org/wiki/Perfect_number), [abundant](https://en.wikipedia.org/wiki/Perfect_number), or [deficient](https://en.wikipedia.org/wiki/Deficient_number). This is based on the classification scheme for natural numbers by [Nicomachus](https://en.wikipedia.org/wiki/Nicomachus).
This should then be exposed via an API using HTTP&JSON to the user.

### Perfect, Abundant, and Deficient ###

Whether a number is one of these three categories is based on that number's [aliquot sum](https://en.wikipedia.org/wiki/Aliquot_sum). The aliquot sum is calculated by the sum of the divisors or factors of a number, not including the number itself.

For example, the proper divisors of 15 (that is, the positive divisors of 15 that are not equal to 15) are 1, 3 and 5, so the aliquot sum of 15 is 9 (1 + 3 + 5).

* **A perfect number** is where the aliquot sum = number
  * 6 is a perfect number because (1 + 2 + 3) = 6
* **An abundant number** is where the aliquot sum > number
  * 12 is an abundant number because (1 + 2 + 3 + 4 + 6) = 16
* **A deficient number** is where the aliquot sum < number
  * 8 is a deficient number because (1 + 2 + 4) = 7

### Task ###

Write a function to determine whether a given number is perfect, abundant, or deficient:

```php
<?php
function getClassification($integer) {
    // returns 'perfect', 'abundant', or 'deficient'
}
```

Once you have created your function to check a perfect number, we would like you to create an API using HTTP and JSON to expose this functionality. You may structure the API in the way you see fit.

### Tips ###

When completing the task, please think about:

- object-oriented structure
- performance
- exception handling
- testing
- documentation

### How to run the task ###

1- composer install   

2- cp .env.example .env       

3- php artisan key:generate   

4- Create an empty mysql database   

5- Edit your .env file with your database configuration (DB Name, DB User, DB Password)         

6- php artisan passport:install

7- php artisan migrate  

8- php artisan serve then open the browser

9 - To use the API you need to be register. So first you need to hit this endpoint and pass the following values:   

  **POST**:    

  ``127.0.0.1:8000/api/register``   

  _name_  
  _email_  
  _password_  
  _c_password_  


  After register api will return a token.  

  **Copy token and paste it in Autorization header to make api call.**  

  **Example:**  

  Authorization : __Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjA3YjIx ....__  
  Accept: __application/json__  


  **POST**  

  ``127.0.0.1:8000/api/perfectNumber``  

  _number_:  897654567