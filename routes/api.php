<?php

use Illuminate\Http\Request;

Route::post('register', 'API\RegisterController@register');

// To access this endpoint you must be authenticated and have an access token.
Route::middleware('auth:api')->group(function () {
    Route::post('/perfectNumber', 'PerfectNumberController@index');
});
